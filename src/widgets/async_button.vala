class Writer.AsyncButton : Gtk.Button {

    public signal void changed(bool working);
    public bool working { get; private set; }
    public bool grab_parent { get; set; default = false; }

    private Gtk.Stack stack;
    private Gtk.Spinner spinner;
    public Gtk.Box content;

    public AsyncButton(string? label = null) {
        stack = new Gtk.Stack();
        stack.show();

        spinner = new Gtk.Spinner();
        spinner.start();
        spinner.show();

        content = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        content.show();

        working = false;

        stack.add_named(spinner, "spinner");
        stack.add_named(content, "content");

        stack.set_visible_child_name("content");

        if (label != null) {
            var lbl = new Gtk.Label(label);
            lbl.show();
            content.pack_end(lbl, true, false, 0);
        }

        changed.connect((b) => {
            set_sensitive(!b);

            if (grab_parent) {
                Gtk.Widget? parent = get_parent();
                if (parent != null)
                    parent.set_sensitive(!b);
            }

            stack.set_visible_child_name(b ? "spinner" : "content");
        });

        add(stack);
        show();
    }

    public void set_is_working(bool working) {
        if (working != this.working)
            changed(working);
        this.working = working;
    }
}
