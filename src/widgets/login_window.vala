/* login_window.vala
 *
 * Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

[GtkTemplate (ui="/com/raggesilver/Writer/layouts/login_window.ui")]
public class Writer.LoginWindow : Gtk.ApplicationWindow {

    AsyncButton do_login_button;
    AsyncButton do_register_button;

    public signal void response();

    [GtkChild]
    Gtk.Entry name_entry;

    [GtkChild]
    Gtk.Entry email_entry;

    [GtkChild]
    Gtk.Entry password_entry;

    [GtkChild]
    Gtk.Button cancel_button;

    [GtkChild]
    Gtk.Button set_login_stack_button;

    [GtkChild]
    Gtk.Button set_register_stack_button;

    [GtkChild]
    Gtk.Box login_box;

    [GtkChild]
    Gtk.Box register_box;

    [GtkChild]
    Gtk.Stack stack;

    [GtkChild]
    Gtk.Revealer error_revealer;

    [GtkChild]
    Gtk.Label error_label;

    public LoginWindow(Gtk.ApplicationWindow parent) {
        Object(application: parent.get_application());

        set_transient_for(parent);

        cancel_button.clicked.connect(() => {
           destroy();
        });

        do_login_button = new AsyncButton("Login");
        do_login_button.grab_parent = true;
        do_login_button.get_style_context().add_class("suggested-action");

        do_register_button = new AsyncButton("Register");
        do_register_button.grab_parent = true;
        do_register_button.get_style_context().add_class("suggested-action");

        login_box.pack_end(do_login_button, true, true, 0);
        register_box.pack_end(do_register_button, true, true, 0);

        set_login_stack_button.clicked.connect(() => {
            stack.set_visible_child_name("login_actions_page");
            name_entry.hide();
        });

        set_register_stack_button.clicked.connect(() => {
            name_entry.show();
            stack.set_visible_child_name("register_actions_page");
        });

        do_login_button.clicked.connect(on_login);
        do_register_button.clicked.connect(on_register);
    }

    void on_login() {
        do_login_button.set_is_working(true);

        var ht = new HashTable<string, string> (str_hash, str_equal);
        ht.insert("email", email_entry.get_text());
        ht.insert("password", password_entry.get_text());
        string req = hash_table_to_json_string(ht);

        axios.post(@"$API_BASE/auth/login", req, (_, res) => {
            if (res.status_code != 200) {
                error_label.set_text((string) res.response_body.data);
                error_revealer.set_reveal_child(true);
                do_login_button.set_is_working(false);
                return ;
            }
            error_revealer.set_reveal_child(false); // Hide any previous errors
            var tok = (string) res.response_body.data;
            guard.set_token.begin(tok, (_, res) => {
                if (guard.set_token.end(res)) {
                    response();
                    destroy();
                }
            });
        });
    }

    void on_register() {
        do_register_button.set_is_working(true);

        var ht = new HashTable<string, string> (str_hash, str_equal);
        ht.insert("email", email_entry.get_text());
        ht.insert("password", password_entry.get_text());
        ht.insert("name", name_entry.get_text());
        string req = hash_table_to_json_string(ht);

        axios.post(@"$API_BASE/auth/register", req, (_, res) => {
            if (res.status_code != 200) {
                error_label.set_text((string) res.response_body.data);
                error_revealer.set_reveal_child(true);
                do_register_button.set_is_working(false);
                return ;
            }
            error_revealer.set_reveal_child(false); // Hide any previous errors
            var tok = (string) res.response_body.data;
            guard.set_token.begin(tok, (_, res) => {
                if (guard.set_token.end(res)) {
                    response();
                    destroy();
                }
            });
        });
    }
}
