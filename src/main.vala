/* main.vala
 *
 * Copyright 2019 Paulo Queiroz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Writer {
    public bool signed = false;
    Writer.Settings settings;
    Writer.Axios axios;
    Writer.Guard guard;
    Writer.Shell shell;
    string API_BASE;
}

int main (string[] args) {
    Writer.settings = Writer.Settings.get_instance();
    Writer.axios = Writer.Axios.get_instance();
    Writer.guard = Writer.Guard.get_instance();
    Writer.shell = Writer.Shell.get_instance();
    string? _uri = Environment.get_variable("API_BASE");
    Writer.API_BASE = (_uri == null) ? "https://ragge.site/api" : _uri;

    var app = new Gtk.Application("com.raggesilver.Writer",
                                  ApplicationFlags.FLAGS_NONE);

    app.activate.connect (() => {
        var win = app.active_window;
        if (win == null) {
            win = new Writer.WelcomeWindow (app);
        }
        win.show ();
    });

    return app.run (args);
}
