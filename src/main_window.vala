/* window.vala
 *
 * Copyright 2019 Paulo Queiroz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Writer.View : Gtk.SourceView {

    public new Gtk.SourceBuffer buffer;

    public enum MakeType {
        BOLD,
        ITALIC,
        STRIKE,
        UNDERLINE
    }

    public View() {
        Object(top_margin: 20,
               bottom_margin: 20,
               monospace: true,
               pixels_above_lines: 3,
               pixels_below_lines: 3,
               auto_indent: true,
               indent_width: 4,
               insert_spaces_instead_of_tabs: true,
               smart_backspace: true,
               wrap_mode: Gtk.WrapMode.WORD_CHAR);

        // FIXME this draws terribly
        //  space_drawer.set_types_for_locations(
        //      Gtk.SourceSpaceLocationFlags.TRAILING,
        //      Gtk.SourceSpaceTypeFlags.SPACE | Gtk.SourceSpaceTypeFlags.TAB);
        //  space_drawer.set_enable_matrix(true);

        buffer = get_buffer() as Gtk.SourceBuffer;
        var lm = Gtk.SourceLanguageManager.get_default();
        buffer.set_language(lm.get_language("markdown"));
        buffer.set_highlight_syntax(settings.highlight);
    }

    const string[] make_ins = {"****", "**", "~~~~", "<u></u>"};

    public bool do_make(MakeType tp) {

        Gtk.TextIter start, end, it;

        buffer.begin_user_action();

        if (buffer.get_selection_bounds(out start, out end)) {
            var offset = (int) GLib.Math.floor(make_ins[tp].length / 2);

            string text = buffer.get_text(start, end, true);

            buffer.place_cursor(start);

            buffer.insert(ref start, make_ins[tp], offset);
            buffer.get_iter_at_offset(out it,
                                      buffer.cursor_position + text.length);
            buffer.insert(ref it, make_ins[tp].offset(offset),
                          make_ins[tp].length - offset);

            //  it.forward_chars(make_ins[tp].length - offset);
        } else {
            int mid = ceil((float) make_ins[tp].length / 2);
            buffer.insert_at_cursor(make_ins[tp], make_ins[tp].length);
            buffer.get_iter_at_offset(out it,
                buffer.cursor_position - mid);
        }

        buffer.place_cursor(it);
        buffer.end_user_action();

        return false;
    }

    public string get_content() {
        Gtk.TextIter s, e;
        buffer.get_start_iter(out s);
        buffer.get_end_iter(out e);
        return (buffer.get_text(s, e, true));
    }

    public void set_content(string s) {
        buffer.begin_not_undoable_action();
        buffer.set_text(s);
        buffer.end_not_undoable_action();
    }
}

[GtkTemplate (ui = "/com/raggesilver/Writer/layouts/main_window.ui")]
public class Writer.MainWindow : Gtk.ApplicationWindow {

    View view;
    Proton.File? file;

    Gtk.Settings s;

    [GtkChild]
    Gtk.ScrolledWindow scrolled_window;

    [GtkChild]
    Gtk.CheckButton highlight_check_button;

    [GtkChild]
    Gtk.CheckButton dark_mode_check_button;

    [GtkChild]
    Gtk.Button new_article_button;

    [GtkChild]
    Gtk.Button do_bold_button;

    [GtkChild]
    Gtk.Button do_italic_button;

    [GtkChild]
    Gtk.Button do_strike_button;

    [GtkChild]
    Gtk.Button do_underline_button;

    [GtkChild]
    Gtk.Button do_save_button;

    [GtkChild]
    Gtk.Button do_publish_button;

    public MainWindow(Gtk.Application app, Proton.File? _file = null) {
        Object(application: app);

        s = Gtk.Settings.get_default();
        s.gtk_application_prefer_dark_theme = settings.dark_mode;

        var css_provider = new Gtk.CssProvider();
        css_provider.load_from_resource(
            "/com/raggesilver/Writer/resources/style.css");

        Gtk.StyleContext.add_provider_for_screen (Gdk.Screen.get_default(),
            css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        view = new View();
        scrolled_window.add(view);
        scrolled_window.show_all();
        file = _file;

        var accel_group = new Gtk.AccelGroup();
        add_accel_group(accel_group);

        highlight_check_button.set_active(settings.highlight);
        dark_mode_check_button.set_active(settings.dark_mode);

        size_allocate.connect(() => { update_ui(); });

        highlight_check_button.toggled.connect(() => {
            settings.highlight = highlight_check_button.get_active();
            view.buffer.set_highlight_syntax(settings.highlight);
        });

        dark_mode_check_button.toggled.connect(() => {
            settings.dark_mode = dark_mode_check_button.get_active();
            s.gtk_application_prefer_dark_theme = settings.dark_mode;
        });

        string? multiple = GLib.Environment.get_variable("MULTIPLE");
        new_article_button.set_sensitive((multiple == "true"));

        new_article_button.clicked.connect(() => {
            var nw = new MainWindow(this.get_application());
            nw.show();
        });

        // Accels
        accel_group.connect(Gdk.keyval_from_name ("b"),
                            Gdk.ModifierType.CONTROL_MASK,
                            0,
                            () => {
                                return view.do_make(View.MakeType.BOLD);
                            });
        accel_group.connect(Gdk.keyval_from_name ("i"),
                            Gdk.ModifierType.CONTROL_MASK,
                            0,
                            () => {
                                return view.do_make(View.MakeType.ITALIC);
                            });
        accel_group.connect(Gdk.keyval_from_name ("u"),
                            Gdk.ModifierType.CONTROL_MASK,
                            0,
                            () => {
                                return view.do_make(View.MakeType.UNDERLINE);
                            });
        accel_group.connect(Gdk.keyval_from_name ("s"),
                            Gdk.ModifierType.CONTROL_MASK,
                            0,
                            save);

        do_bold_button.clicked.connect(() => {
            view.do_make(View.MakeType.BOLD); });
        do_italic_button.clicked.connect(() => {
            view.do_make(View.MakeType.ITALIC); });
        do_strike_button.clicked.connect(() => {
            view.do_make(View.MakeType.STRIKE); });
        do_underline_button.clicked.connect(() => {
            view.do_make(View.MakeType.UNDERLINE); });
        do_save_button.clicked.connect(() => { save(); });
        do_publish_button.clicked.connect(() => { publish(); });

        open();
    }

    void update_ui() {
        var padd = view.get_allocated_width()*0.10;
        view.left_margin = view.right_margin = (int) padd;
    }

    bool save() {

        if (file == null) {
            var dialog = new Gtk.FileChooserDialog("Save as", this,
                Gtk.FileChooserAction.SAVE,
                "Cancel", Gtk.ResponseType.CANCEL,
                "_Open", Gtk.ResponseType.OK,
                null);
            if (dialog.run() == Gtk.ResponseType.OK) {
                file = new Proton.File(dialog.get_filename());
                dialog.destroy();
                return (save());
            }
            dialog.destroy();
        } else {
            settings.add_recent(file.path);
            file.write_async.begin(view.get_content(), (obj, res) => {
                file.write_async.end(res);
                stdout.printf("Saved\n");
            });
        }

        return false;
    }

    void open() {
        if (file == null)
            return ;
        settings.add_recent(file.path);
        file.read_async.begin((obj, res) => {
            string? content = file.read_async.end(res);
            if (content != null) {
                view.set_content(content);
            }
        });
    }

    void publish() {
        // Publishing has an ID which is <email><filepath>
        // If there already is a publication with that id
        // that article will be updated

        if (file == null) {
            save();
            return ;
        }

        string content = view.get_content();

        var pw = new PublishWindow(this, file, content);
        pw.show();
    }
}
