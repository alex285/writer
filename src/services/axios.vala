public class Writer.Axios : Object {
    static Axios instance = null;

    public signal void error(Soup.Message msg);

    public Soup.Session session { get; private set; }
    public string? token = null;

    private Axios() {
        session = new Soup.Session();
    }

    public void post(string url, string request, Soup.SessionCallback? cb) {
        Soup.Message msg = new Soup.Message("POST", url);
        msg.set_request("application/json", Soup.MemoryUse.COPY, request.data);

        if (token != null)
            msg.request_headers.append("authorization", @"Bearer $token");

        session.queue_message(msg, (sess, res) => {
            if (res.status_code >= 400)
                error(res);
            cb(sess, res);
        });
    }

    public async Soup.Message post_async(string url, string request) {
        Soup.Message msg = new Soup.Message("POST", url);
        msg.set_request("application/json", Soup.MemoryUse.COPY, request.data);

        if (token != null)
            msg.request_headers.append("authorization", @"Bearer $token");

        try {
            var is = yield session.send_async(msg);
            uint8 buff[200];
            string body = "";

            while (is.read(buff) > 0)
                body += (string) buff;

            msg.set_response("text/plain", Soup.MemoryUse.COPY, body.data);
        } catch (GLib.Error e) {
            msg.set_response("text/plain", Soup.MemoryUse.COPY, null);
            warning(e.message);
        }

        if (msg.status_code >= 400)
            error(msg);

        return (msg);
    }

    public static Axios get_instance() {
        if (instance == null)
            instance = new Axios();
        return (instance);
    }
}
