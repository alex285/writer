public class Writer.Shell {
    static Shell? instance = null;

    Shell() {}

    public int run(string command, out string stdout, out string stderr) {
        int res = -1;
        try {
            Process.spawn_sync("/", command.split(" "),
                Environ.get(), SpawnFlags.SEARCH_PATH, null, out stdout,
                out stderr, out res);
        } catch {}
        return (res);
    }

    public static Shell get_instance() {
        if (instance == null)
            instance = new Shell();

        return (instance);
    }
}
